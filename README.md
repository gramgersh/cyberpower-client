# cyberpower-client

This is a script and configuration file that will query a remote server running the [PowerPanel Personal Linux](https://www.cyberpowersystems.com/product/software/powerpanel-for-linux/) software, and shut down the local system in an orderly fashion upon a power event.

This script has been written under CentOS 7 using bash 4.2.46.

See [LICENSE.txt](LICENSE.txt) for the GPL 2 License.

## Prepare the CyberPower Monitoring server.

In these examples, I have a CentOS 7 Linux system running the [PowerPanel Personal Linux](https://www.cyberpowersystems.com/product/software/powerpanel-for-linux/) software.

I have named this server 'cyberpower1'.

### Select User for Remote Access

To run these script, each client must be able to ssh to the cyberpower1 server in order to run the `pwrstat -status` command.

How you do this will depend on your security policy.  For my system, I did the following:

* create a custom script that runs `pwrstat -status`
* create a new pwrstat user, setting the pwrstat user's shell to the custom script
* add a sudoers entry for the pwrstat user
* add ssh public keypair to the pwrstat user's `authorized_keys` file.

There are many guides on the internet on how to set up ssh access on Linux systems.

If you do not want to use a custom shell script as the shell, you can use a standard shell and prefix the ssh authorized_keys file to prepend a command (see below).  If you are adding the keys to the `root` account, it is highly recommended that you limit the command for the supplied ssh key.

#### Custom Script

This script is installed as /etc/pwrstatd-shell.sh:

```bash
#!/bin/bash
#
# VERSION=1.0
#
# This is a shell that can be used for the pwrstat user that simply
# returns the status.
exec /usr/bin/sudo /usr/sbin/pwrstat -status
exit 1
```

#### Create a New pwrstat User

```bash
useradd -c "CyberPower Status User" -l -m -s /etc/pwrstatd-shell.sh pwrstat
```

#### Add a sudoers Entry for the pwrstat User

The path to the file may be different on non-CentOS 7 systems:

```bash
visudo /etc/sudoers.d/pwrstat

pwrstat ALL=(ALL) NOPASSWD: /usr/sbin/pwrstat -status
```

#### Add ssh public keypair

Generate a password-less ssh keypair on each 'client' and add the public half to the `~pwrstat/.ssh/authorized_keys` file on the cyberpower1 server.

Again, the command to generate this may differ depending on your security policy.

```bash
ssh-keygen -t rsa -b 4096 -N '' -f /root/.ssh/cyberpower1_rsa
cat /root/.ssh/cyberpower1_rsa.pub
```

Append the contents of the `cat` command above to `~pwrstat/.ssh/authorized_keys` on the cyberpower1 host.  There are many guides on the internet for setting up ssh access.

If you do NOT want to use the custom shell, you can customize the authorized_keys file and prefix the key with:

```bash
command="/usr/bin/sudo /usr/sbin/pwrstat -status"
```

Remove the '/usr/bin/sudo' part if you are adding this to the root user's authorized_keys file.

## Install Software on Client

Download and extract the software on the client into /usr/local:

```bash
cd /usr/local
	
curl -s https://bitbucket.org/gramgersh/cyberpower-client/get/master.tar.gz | \
    tar xzf - --wildcards --strip-components=1 \
        --backup --suffix=.$(date "+%Y%m%d-%H%M") */bin */etc
```

## Create A Low-Battery Script

Create a script to run when a Low-Battery alert is triggered.  To simply run a shutdown, you can copy the `/usr/local/bin/rpwrstat-lowbatt.sh.example` script to `/usr/local/bin/rpwrstat-lowbatt.sh` and set the execute bit.

## Create a configuration file.

In the `/usr/local/etc` directory, you can copy the `rpwrstat.cfg.sample` to `rpwrstat.cfg` and edit it to match your requirements.

Make sure to set the identity file created above in the script.  For the keypair generated above, I would set the following line:

```bash
RPWRSTAT_IDENTITY_FILE=/root/.ssh/cyberpower1_rsa
```

## Test Your Configuration

Test your configuration by running the following command:

```bash
# /usr/local/bin/rpwrstat -n
State:    Normal
Capacity: 100% (thresh=50%)
Runtime : 71 min (thresh=10 min)
```

The first time you run it, you may need to add your host to the ssh `known_hosts` file.


## Create a cron job

Create a cron job as appropriate to run the script.  On a CentOS 7 system, your file can look like the following, which will run a check every minute:

```bash
cat /etc/cron.d/rpwrstat

* * * * * root /usr/local/bin/rpwrstat
```

## Help

You can run the following command to see the available arguments.  If you do not want to set up a config file, you can simply supply all of the necessary arguments in the cron job.

```bash
/usr/local/bin/rpwrstat -h
```
