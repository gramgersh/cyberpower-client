all:

clean:
	find . \( -name '*~' -o -name '#*' -o -name '.*.swp' -o -name ',*' \) -print0 | xargs -0 /bin/rm -f

check: clean
	@for i in bin/* ; do \
		printf "Checking $$i ... " ; \
		/bin/bash -n $$i || exit 1 ; \
		echo "OK" ; \
	done
